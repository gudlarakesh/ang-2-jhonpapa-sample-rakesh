import { Component, EventEmitter, Input, Output, OnInit, OnDestroy } from '@angular/core';
import { CustomerComponent }  from "./customer.component";
import { CustomerService }  from "./customer.service";

@Component({
    selector: 'my-customers',
    templateUrl: 'app/customer/customers.component.html',
    directives: [CustomerComponent],
    providers: [CustomerService],
    styleUrls: [],
    pipes: []
})

export class CustomersComponent implements OnInit, OnDestroy {
  customers: any[];
  constructor(private _customerService :CustomerService) {}

  ngOnInit() {
    this.customers = this._customerService.getCustomers();
  }

  ngOnDestroy() {
  }

}
