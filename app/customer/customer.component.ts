import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
    selector: 'my-customer',
    templateUrl: 'app/customer/customer.component.html',
    directives: [],
    providers: [],
    styleUrls: [],
    pipes: []
})

export class CustomerComponent implements OnInit {
@Input()
customer: {id: number, name: string};
myColor = "blue";
constructor(){}

ngOnInit(){}
}
