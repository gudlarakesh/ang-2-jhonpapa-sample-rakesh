import { Injectable } from '@angular/core';

export interface IntefaceName {
}

@Injectable()
export class CustomerService {

  constructor() {
  }

  getCustomers(){
    return [
      {id : 1, name:"rakesh"},
      {id : 2, name:"Pavan"},
      {id : 3, name:"kiran"},
      {id : 4, name:"souri"}
    ];
  }

}
