import { Component } from '@angular/core';
import { CustomersComponent }  from "./customer/customers.component";

@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    directives: [CustomersComponent]
})
export class AppComponent {
  title = "Customer App";
  name = "kiran";
  wardColor = "green";

  changeSuitColor(){
    this.wardColor = this.wardColor === 'green' ? 'red' : 'green';
  }
}
